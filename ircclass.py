import socket, sys, time, ssl

# This very simple library for IRC is all you need for making an IRC bot
class IRC:
    irc = socket.socket()
    def __init__(self):
        # self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def quote(self, command):
        self.irc.send(bytes(command + "\n", "UTF-8"))

    def send(self, channel, msg):
        self.irc.send(bytes(f"PRIVMSG {channel} :{msg}\n", "UTF-8"))

    def get_response(self):
        time.sleep(1)
        resp = self.irc.recv(2040).decode("UTF-8")

        if resp.find('PING') != -1:
            self.irc.send(bytes(f"PONG {resp.split()[1]}\r\n", "UTF-8"))

        return resp


    def connect(self, server, port, channel, botnick, secure=None):
        print("Connecting to: " + server)
        if port == 6667 or secure == False:
            self.irc = self.s
            self.irc.connect((server, port))
            print("Insecure connection found (6667)")
        elif port == 6697 or secure == True:
            self.irc = ssl.wrap_socket(self.s, ca_certs="/etc/ssl/cert.pem", cert_reqs=ssl.CERT_REQUIRED)
            self.irc.connect((server, port))
            print("Secure connection found (6697)")

        self.irc.send(bytes(f"USER {botnick} {botnick} {botnick} :python\n", "UTF-8"))
        self.irc.send(bytes(f"NICK {botnick}\n", "UTF-8"))
        print("Server joined")
        time.sleep(5)
        self.irc.send(bytes(f"JOIN {channel}\n", "UTF-8"))
        print("Channel joined")
