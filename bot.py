from ircclass import IRC
import json, feedparser, time
from config import delay, server, port, channel, botnick

irc = IRC()
irc.connect(server, port, channel, botnick)

time.sleep(20) # Initial connection to the server might take a while

while True:
    urls = json.loads(open("urls.json").read())
    for url in urls:
        try:
            d = feedparser.parse(url)
            latest = d.entries[0]
            try:
                time.struct_time(urls[url])
            except:
                print("Invalid time found")
                urls[url] = json.loads(json.dumps(latest.updated_parsed))

            if latest.updated_parsed > time.struct_time(urls[url]):
                urls[url] = latest.updated_parsed
                print(f"{latest.link} | {latest.title}")
                irc.send(channel, f"{latest.link} | {latest.title}")
            else:
                print(f"No new entry in {d.feed.title}")
        except:
            print(f"{url} is an invalid link")

    open("urls.json", "w").write(json.dumps(urls, indent=4))
    time.sleep(delay)
